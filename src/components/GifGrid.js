import React from 'react'
import { useFetchGifs } from '../hooks/useFetchGifs'
import { GifGridItem } from './GifGridItem';

export const GifGrid = ({ category }) => {

    
    const {data, loading} = useFetchGifs(category);

    return (
        <div className='card-grid'>
            <h3 className='animate__animated animate__bounce'> { category } </h3>

            {loading && <p>Loading</p>}
            
            <div className='card-grid'>
                
                {
                    data.map( img => 
                        <GifGridItem 
                        key={ img.id }
                        {...img} />
                        )
                    }
        
            </div>
        </div>
    )
}
